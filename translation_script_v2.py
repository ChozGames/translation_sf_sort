#!/usr/bin/python

import sys
from termcolor import colored

def getHelp():
    print('How to use yaml translation sortered ?')
    print('py ' + colored('command ', 'cyan') + colored('[args]', 'yellow'))

def fileSortByLine():
    fichier = open(sys.argv[1], "r")
    datas = fichier.readlines()
    fichier.close()

    arrayToSort = []

    for data in datas:
        arrayToSort.append(data)

    arraySorted = arrayToSort.sort()

    file = open("result_v2.yaml","w")

    for data in arrayToSort:
        file.write(data)
    file.close()

if len(sys.argv) > 1:
    fileSortByLine()
else:
    getHelp()

