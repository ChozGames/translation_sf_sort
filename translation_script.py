#!/usr/bin/python

import sys

fichier = open(sys.argv[1], "r")
datas = fichier.readlines()
fichier.close()

arrayToSort = []

for data in datas:
    dataValue = data.split(':')
    dataSplit = dataValue[0].split('.')
    arrayToSort.append([dataSplit, dataValue[1]])

arrayToSort = sorted(arrayToSort, key=lambda x: x[0][0])

def getStrOfArray(datas):
    res = ""
    for data in datas:
        if res == "":
            res = res + data
        else:
            res = res + "." + data

    return res


file = open("result.yaml","w")

for data in arrayToSort:
    file.write(getStrOfArray(data[0]) + ":" + data[1])
file.close()

# arrayTampon = []
# arrayFinal = []
# arrayFinalTampon = []
# typeTrans = ""
# for data in arrayToSort:

#     if data[0] == typeTrans:
#         if len(data) > 1:
#             arrayTampon.append(data)
#         else:
#             arrayFinal.append(data)
#     else:
#         arrayTamponSort = sorted(arrayTampon, key=lambda x: x[1])
#         arrayFinalTampon = arrayFinalTampon + arrayTamponSort
#         typeTrans = data[0]

# arrayFinal = arrayFinal + arrayFinalTampon

# for data in arrayFinal:
#     print(arrayFinal)


# Récupérer toutes les données dans une tableau
# Le tableau doit etre la forme "une ligne par trad" 
# et dans cette ligne un tableau avec un element par "."x